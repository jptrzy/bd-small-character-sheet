# Blades in the Dark Small Character Sheet

## Goals
Small (A5 Format) and readable character sheet for Blades in the Dark game.

## F&Q
### What about class specific character differences?
Because of the original way that BD presents their character sheets,
it might look like classes differ a lot,
but it isn't true.

There only 5 variables at play:
- **Default Abilities** - Each class have predefined/free abilities,
but players can easy fill them out themselves.
- **Special Abilities** - You will need to write them yourself.
- **Dangerous friends** - We just ignore this field.
You can write them in your notes.
- **Special Equipment** - There is a special place for it on this card.
- **Standard Equipment** - Everyone have the same basic equipment.
*There are only two classes with minor changes to it,
so we can ignore them.

## TODO
- [ ] Play test
- [ ] Add English translation instead of only polish one
- [X] Clean up spaghetti code

