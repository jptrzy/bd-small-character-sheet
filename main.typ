#let DEBUG = false

#let black_prim = black.lighten(4%)
#let black_secc = black_prim.lighten(80%)

#let orange_prim = orange.lighten(50%)
#let orange_secc = orange.lighten(88%)

#if DEBUG {
  black_prim = red
  black_secc = red.lighten(80%)

  orange_prim = green.lighten(40%)
  orange_secc = green.lighten(80%)
}

#let color_sec = orange.lighten(80%)

#let DATA = toml("lang/en.toml")

/*****************************************************************************/
/* Global Settings                                                           */
/*****************************************************************************/

#set page(
  paper: "a5",
  number-align: center,
  flipped: true,
  margin: (
    y: 10pt,
    x: 10pt,
  )
)

#set text(
  font: "Noto Sans",
  tracking: 0.07em,
  spacing: 0.4em,
  size: 11pt,
)

/*****************************************************************************/
/* Macros                                                                    */
/*****************************************************************************/

/*
* Creates a title with a empty fields in the shape of a blade
*
* title - text of the title
* blades - how many fields will be created
*/
#let bladed(title, blades: 5) = {
  grid(
    inset: (bottom: -0.34em),
    columns: blades * 2 + 1,
    text(
      box(
        fill: black_prim,
        inset: 0.2em,
        stroke: 0.1em + black_prim,
        width: 100%,
      )[
        #upper[*#title*] 
      ],
      fill: white
    ),
    ..(
      polygon(
        fill: white,
        stroke: 0.1em + black_prim,
        (0em, 0em),
        (.7em, 0em),
        (.7em, 1.2em),
        (0em, 1.5em),
      ),
      box(
        fill: black_prim,
        inset: 0.125em,
        stroke: 0.1em + black_prim,
        width: 0.4em,
        height: 1.114em
      ),
    ) * blades,
  )
}

/*
* Creates an block of abilities and their level describing fields
* 
* Eg:
* ○ ○ ○ ○ Ability 1
* ○ ○ ○ ○ Ability 2
* ○ ○ ○ ○ Ability 3
* ○ ○ ○ ○ Ability 4
*
* names - list of abilities names
*/
#let abilities(names: ()) = {
  grid(
    align: horizon,
    columns: 5,
    rows: 4,
    row-gutter: 0.6em,
    column-gutter: 0.3em,
    ..names.map(text.with(
      size: .8em,
      fill: black_prim,
    )).map(upper).map(grid.cell.with(
      x: 4,
      inset: (left: 0.8em),
    )),
    ..(
      circle(
        radius: .4em,
        fill: orange_secc
      ),
    ) * 16,
  )
}


/*
* Creates a empty slots fields with tool name
*
* Eg.
* ▢  First tool
*
* ▢ ▢ ▢  Second tool
* 
* slots - how many slots
* name - tool's name
* empty - if it will have a writable field for a name
*/
#let tool(slots: 1, name: "", empty: false) = {
  let fill = black_prim
  if empty {
    fill = black_secc
  }
  box[
    #set text(
      tracking: 0.02em,
      spacing: 0.4em,
    )

    #grid(
      align: horizon,
      columns: slots + 1,
      ..range(slots).map((i) => 
        grid(
          columns: 2,
          square(size: 0.8em, stroke: fill),
          if i != slots - 1 [
            #rect(
              width: .3em,
              height: .4em,
              fill: fill.lighten(20%),
              stroke: (x: fill),
            )
          ] else [
            #square(
              size: .4em,
              stroke: none,
            )
          ],
        )
      ),
      if empty {
        block(
          width: 100%,
          height: 0.8em,
          outset: (bottom: 0.3em),
          stroke: (
            bottom: (paint: black_secc, dash: "dashed")
          )
        )
      } else {
        text(fill: fill)[#name]
      }
    )
  ]
}

/*****************************************************************************/
/* Big Sub Elements                                                          */
/*****************************************************************************/

/*
*  TODO DECS
*/
#let loadout() = {
  box(
    fill: orange_prim,
    width: 100%,
    inset: (top: .1em),
    outset: (left: .1em),
    baseline: 1em,
  )[
    #box[]
    #for i in DATA.loadout.map((i) => [*#i*]) [
      #box(baseline: .3em)[
        #polygon(
          fill: white,
          stroke: .14em + black_prim,
          (.5em, -.2em),
          (1em, .5em),
          (.5em, 1.2em),
          (0em,  .5em),
        )
      ]
      #text(fill: black_prim)[#i]
    ]
  ]
}

/*
*  TODO DECS
*/
#let harm(slots: 1, name: "", empty: false) = {
  table(
    align: center + horizon,
    stroke: (x, y) => (
    // Separate black cells with white strokes.
      x: if x == 0 or x == 2 { orange_prim },
      y: if y > 1 { black_prim },
    ),
    fill: (x, _) => if x == 0 or x == 2 { orange_prim },
    columns: (auto, 1fr, auto),
    rows: (auto, 2.4em, 1.9em),
    table.cell(
      colspan: 3,
      align: left,
      box(
        width: 100%,
        outset: .5em,
        fill: black_prim
      )[
        #text(fill: white, size: .8em)[
          #upper[
            *#DATA.harm*
          ]
        ]
      ]
    ),
    ..for key in (
      (3, upper[*#DATA.harms.at(2)*], 1),
      (2, upper[*#DATA.harms.at(1)*], 2),
      (1, upper[*#DATA.harms.at(0)*], 2),
    ) {
      (
        table.cell(
          x: 0,
          rowspan: key.at(2),
          text(fill: black_prim)[*#key.at(0)*],
        ),
        table.cell(
          x: 2,
          rowspan: key.at(2),
          rotate(-90deg, reflow: true)[
            #text(
              size:.5em,
              fill: black_prim,
            )[
              #key.at(1)
            ]
          ],
        ),
      )
    },
  )
}

/*
*  TODO DECS
*/
#let ____(slots: 1, name: "", empty: false) = {
}

/*****************************************************************************/
/* Main Page                                                                 */
/*****************************************************************************/

#let main() = {
  grid(
    columns: (1fr, 1fr, 1fr),
    rows: 2,
    gutter: 0.7em,
    align: horizon,
    text(
      tracking: 0em,
      spacing: 0.4em,
      fill: black_prim,
    )[#upper[
      *#DATA.title by #link("https://jptrzy.xyz")[jptrzy]*
    ]],
    ..for _text in (DATA.name, DATA.class, DATA.heritage, DATA.background, DATA.vice) {
      (
        block(
          stroke: (
            bottom: orange_prim
          ),
          width: 100%,
          inset: (top:0.4em, bottom: 0.6em),
        )[
          #text(
            fill: black_secc,
          )[
            #_text
          ]
        ],
      )
    },
  )
  
  grid(
    columns: (1fr, 1fr/2, 1fr),
    rows: 2,
    gutter: 0.6em,
    bladed(blades: 9)[
      #DATA.stress
    ],
    bladed(blades: 4)[
      #DATA.trauma
    ],
    bladed(blades: 8)[
      #DATA.playbook
    ],
  )
  
  
  grid(
    columns: 3,
    rows: 2,
    gutter: .6em,
    bladed[
      #DATA.blades.at(0)
    ],
    bladed[
      #DATA.blades.at(1)
    ],
  
    bladed[
      #DATA.blades.at(2)
    ],
 
    ..for b in DATA.abilities {
      (abilities(names: b),)
    },
  )
  
  grid(
    columns: (1fr, 1fr/2.08, 1fr),
    gutter: .6em,
    block[
      #box[
        #block(
          stroke: (bottom: orange_secc),
          width: 100%,
          height: 1.3em,
        )[
          #text(
            size: .8em,
            fill: black_secc
          )[#DATA.special]
        ]
    
        #for _ in range(2) [
          #block(
            stroke: (bottom: orange_secc),
            width: 100%,
            height: 1em,
          )
        ]
      ]
  
      #block(
        width: 100%,
        height: 1em,
        stroke: (
          bottom: black_secc
        ),
      )[#text(fill: black_secc, size: .8em)[#DATA.notes]]
      #for _ in range(6) [
        #block(
          width: 100%,
          height: .82em,
          stroke: (
            bottom: black_secc
          )
        )
      ]
    ],
    block(width: 100%)[
      #block(
      )[
        #show par: set block(spacing: .35em)
  
        #show DATA.replace: _ => box[
          #box(image(
            "gear.svg",
            height: 0.8em,
          ))
        ]
  
        #loadout()
  
        #v(.4em)
 
        #for i in DATA.tools {
          par[
            #if i.len() > 2 {
              box(
                stroke: (left: .1em + black_prim),
                outset: (left: .2em)
              )[
                #tool(slots: i.at(0), name: i.at(1))

                #tool(slots: i.at(2), name: i.at(3))
              ]
            } else {
              tool(slots: i.at(0), name: i.at(1))
            }
          ]
        }
          
        /*
        #box(
          stroke: (left: .1em + black_prim),
          outset: (left: .2em)
        )[
          #tool(slots: 2, name: "Pancerz")
  
          #tool(slots: 3, name: "Ciężki")
        ]
  
        #box(
          stroke: (left: .1em + black_prim),
          outset: (left: .2em)
        )[
          #tool(slots: 1, name: "Pistolet")
  
          #tool(slots: 1, name: "Drugi Pistolet")
        ]
        */
      ]
    ],
    block[
      #harm()
  
      #show par: set block(spacing: .5em)
      #for _ in range(6) [
        #par[
          #tool(slots: 3, name: "", empty: true) 
        ]
      ]
    ]
  )
}
  
#main()
