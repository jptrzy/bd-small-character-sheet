
#set page(
  paper: "a4",
  number-align: center,
  flipped: false,
  margin: (
    y: 10pt,
    x: 10pt,
  )
)

#set text(
  font: "Noto Sans",
  tracking: 0.07em,
  spacing: 0.2em,
  size: 11pt,
)

#import "main.typ"

#main.main()

#layout(size => {
  let half = 50% * size.width


  let cells = 35
  let cell = size.width / (cells)
  let y = calc.fract(size.height / cell) * cell

  [
    #let pat = pattern(size: (cell, cell))[
      #place(
        dx: cell / 2,
        dy: cell / 2 - y / 2,
        rotate(45deg, square(
          size: 1pt,
          fill: gray,
        )),
      )
    ]

    #v(20pt)

    #rotate(180deg)[
      #link("https://jptrzy.xyz")[
        #text(gray, 8pt)[https://jptrzy.xyz]
        #h(.7em)
      ]
    ]

    #rect(fill: pat, width: 100%, height: 44%)
  ]
})
