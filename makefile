

img: build
	mkdir -p image
	convert -background white -flatten -quality 100 -density 128 main.pdf image/BD-CS-A5.webp
	convert -background white -flatten -quality 100 -density 128 dual.pdf image/BD-CS-A4.webp

build:
	typst compile main.typ
	typst compile dual.typ
